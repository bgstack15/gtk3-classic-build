# File: Makefile
# Location: https://gitlab.com/bgstack15/gtk3-classic-build
# Author: bgstack15
# Startdate: 2021-08-04 09:51
# Title: Makefile for gtk3-classic-build debuild task
# Improve:

all: gtk3

gtk3:
	./gtk-classic-build-deb.sh

clean:
	rm -rf ./debian_vers ./classic_vers ./gtk+-3.24*/
	rm -rf gtk+3.0_*z gtk+3.0_*.build* gtk+3.0_*.changes* gtk+3.0_*.dsc ./libgtk-3*deb ./libgail-3*deb ./gtk3classic/
