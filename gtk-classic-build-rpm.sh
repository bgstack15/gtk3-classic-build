#!/bin/sh
# File: gtk-classic-build-rpm.sh
# Location: https://gitlab.com/bgstack15/gtk3-classic-build
# Author: bgstack15
# Startdate: 2021-08-18 08:06
# SPDX-License-Identifier: GPL-3.0
# Title: Build src rpm for gtk3-classic
# Purpose: build git repo and src.rpm for gtk3-classic
# History:
# Usage:
#    ./gtk-classic-build.rpm
# References:
#    rpm fedpkg-minimal
# Improve:
#    Handle other versions of gtk other than only what is installed on dev system.
# Dependencies:
#    git, yum | dnf

test -z "${WORKDIR}" && WORKDIR="$( readlink -f . )"
export WORKDIR

GTK3CLASSIC_GIT=https://github.com/lah7/gtk3-classic
FEDORA_GTK3_GIT=https://src.fedoraproject.org/rpms/gtk3
cd "${WORKDIR}"

#### Find versions of gtk3-classic available, which involves fetching the repo
if ! test "$( cd "${WORKDIR}/gtk3classic" 2>/dev/null && git remote -v | grep -o 'origin  https://github.com/lah7/gtk3-classic' | head -n1 )" = "origin  https://github.com/lah7/gtk3-classic" ; then
   git clone "${GTK3CLASSIC_GIT}" "${WORKDIR}/gtk3classic"
else
   (
      cd "${WORKDIR}/gtk3classic"
      git checkout master
      git pull --all
   )
fi
classic_vers="$( cd "${WORKDIR}/gtk3classic" ; git tag | sort -r --sort=version | head -n 20 )"
echo "${classic_vers}" > "${WORKDIR}/classic_vers"

### Find versions of gtk3 available to Fedora Linux
fedora_vers="$( yum list installed gtk3 | awk '$1~/gtk/{print $2,$2}' | awk '{gsub("-[0-9]+.fc[0-9]+","",$1);print}' | sort -r --sort=version )"
echo "${fedora_vers}" > "${WORKDIR}/fedora_vers"

### Find highest matching version
highest_ver="$( awk 'NR==FNR{a[$0];next} NR!=FNR{if($1 in a){print}}' "${WORKDIR}/classic_vers" "${WORKDIR}/fedora_vers" | head -n1 )"
raw_version="$( echo "${highest_ver}" | awk '{print $1}' )"
fedora_version="$( echo "${highest_ver}" | awk '{print $2}' | awk -F'-' '{print $2}' | sed -r -e 's/fc/f/g;' | awk -F'.' '{print $2}' )" # fedora gtk3 package does not use git tags for gtk3 versions; it uses branch names for each release of Fedora, so f33.
fedora_git="gtk3" # the name of this directory must be gtk3 so `fedpkg sources` works correctly

### Fetch Fedora Linux sources
cd "${WORKDIR}"
git clone "${FEDORA_GTK3_GIT}" "${fedora_git}"
cd "${WORKDIR}/${fedora_git}"
git checkout "${fedora_version}"

### Fetch gtk3-classic sources
(
   cd "${WORKDIR}/gtk3classic"
   git checkout "${raw_version}"
)

### Protect against uncleaned directory
if test -f "gtk3.spec.orig" ;
then
   cp -pr gtk3.spec.orig gtk3.spec
else
   cp -pr gtk3.spec gtk3.spec.orig
fi
### Combine
sed -i -r -e '/Release:/{s/[0-9]+%/100%/}' gtk3.spec
{
   sed -n -r -e '1,/%changelog/p' gtk3.spec
   echo "* $( date "+%a %b %d %Y" ) B. Stack <bgstack15@gmail.com> - ${raw_version}-100"
   echo "- Rebuild gtk3 with gtk3-classic patches"
   echo "- Remove tests"
   echo ""
   sed -r -e '1,/%changelog/d' gtk3.spec
} > gtk3.spec.1
cp -p "${WORKDIR}/gtk3classic/"*.patch "${WORKDIR}/${fedora_git}"
# build patch list
patch_list="$( grep -viE "^\s*(#|$)" "${WORKDIR}/gtk3classic/series" )"
case "${raw_version}" in
   3.24.29)
      # Fedora has already applied:
      # appearance__buttons-menus-icons.patch
      # 3.24.29 just does not need:
      # appearance__smaller-statusbar file-chooser__places-sidebar
      remove_list="appearance__smaller-statusbar file-chooser__places-sidebar appearance__buttons-menus-icons"
      for word in ${remove_list} ; do rm ${word}.patch ; done
      patch_list="$( echo "${patch_list}" | grep -v $( for word in ${remove_list} ; do printf '%s %s ' '-e' "${word}" ; done ) )"
      #echo "patch_list=\"${patch_list}\""
      ;;
   *) echo "Unknown patchset for ${raw_version}. Using all patches." ;;
esac
# add %patch macros
{
   # print everything up to %description, excluding description
   sed -n -r -e '1,/%description/p' gtk3.spec.1 | head -n -1
   echo "BuildRequires: libjpeg-turbo-devel"
   # adapt the dpkg series file contents for an rpm spec file
   x=799
   for word in ${patch_list} ;
   do
      x=$((x+1))
      echo "Patch${x}: ${word}"
   done
   # print the next section up to but excluding %build
   sed -n -r -e '/%description/,/%build/p' gtk3.spec.1 | head -n -1
   x=799
   for word in ${patch_list} ;
   do
      x=$((x+1))
      echo "%patch${x} -p1"
   done
   sed -n -r -e '/%build/,$p' gtk3.spec.1
} > gtk3.spec.2
# Remove tests
{
   sed -r -e '/%package tests/,/the functionality of the installed/d' -e '/ble-installed-tests/s/enable-/disable-/g;' -e '/%files tests/,/%changelog/{/%changelog/!d;}' gtk3.spec.2
} > gtk3.spec.3
mv gtk3.spec.3 gtk3.spec
rm gtk3.spec.[12]
# that should be everything

### We can now upload this to a public git repo
cd "${WORKDIR}/${fedora_git}"
git remote add gitlab https://gitlab.com/bgstack15/gtk3-classic-build-gtk3.git
git add gtk3.spec *.patch
cp -pf "${WORKDIR}/for-repo/README.md" README.md
grep -qE '\*\.spec\.orig' .gitignore || echo "*.spec.orig" >> .gitignore
grep -qE '\.\*\.swp' .gitignore || echo '.*.swp' >> .gitignore
git pull
git add .

### Build srpm
# download all the sources from the sources file
# or we could have used spectool -g gtk3.spec here.
fedpkg sources
mkdir -p ~/rpmbuild/SOURCES
mv -f *.tar.xz ~/rpmbuild/SOURCES
rpmbuild -bs gtk3.spec
cp -pr ~/rpmbuild/SRPMS/gtk3-${raw_version}-100.*.src.rpm "${WORKDIR}/"

### Clean
test -z "${NO_CLEAN}" && rm -rf "${WORKDIR}/fedora_vers" "${WORKDIR}/classic_vers" "${WORKDIR}/gtk3classic"
# do not remove ${fedora_git} because we need to push updates to gitlab remote, manually
