# Readme for gtk3-classic-build
Gtk3-classic-build-deb is [bgstack15](https://bgstack15.wordpress.com)'s builds of [gtk3-classic](https://github.com/lah7/gtk3-classic) for Devuan Ceres. Check out the final assets at my [OBS sub-project](https://build.opensuse.org/project/show/home:bgstack15:gtk3-classic).

Gtk3-classic-build-rpm is by the same author, for Fedora. Check out the final assets on my [COPR](https://copr.fedorainfracloud.org/coprs/bgstack15/gtk3-classic/package/gtk3/).

## Overview
Notable improvements over stock gtk3 include
* Regular type-to-navigate-to-filename in the file dialog instead of the "typeahead" behavior
* CSDs are removed, which actually could deprecate [gtk3-nocsd](https://packages.debian.org/sid/gtk3-nocsd) (official upstream Debian package)

Notable weaknesses include:
* Always-on mnemonics is still not implemented. Keep using [gtk3-automnemonics](https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=gtk3-automnemonics) for that.

## Using
### deb script
The `gtk3-classic-build-deb.sh` script and Makefile can be used to generate the build assets that can be used to build the binary dpkgs of gtk3 with the gtk3-classic patches.

The shell script finds the available gtk3 versions in debian, and gtk3-classic releases, and then finds the highest version that matches between the two. This highest version then gets downloaded, given the patches in `debian/patches/series`, and then the .dsc and .debian.tar.xz file are generated!

An optional way to use it with `deploy-to-obs.sh`. If you need to use a custom committish for any value, define it in an environment variable.

    RAW=3.24.34 CLASSIC=defuzz-3.24.34 DEB=3.24.34-3 sh -x ./deploy-to-obs.sh

### rpm script
The `gtk-classic-build-rpm.sh` script can be used to generate a git repository contents that can be used by rpkg on the COPR. This script also generates a src.rpm which can be used to build an rpm locally.

## Upstreams
### Build script
The deb build script is loosely inspired by [luigifab/deb.sh](https://gist.github.com/luigifab/0fce786cdb93b5687069a82f490ea95e) (gist.github.com), but is maintained separately.

The rpm build script is loosely inspired by my script for [LibreWolf rpm](https://gitlab.com/bgstack15/librewolf-fedora) which makes its own [git repo](https://gitlab.com/bgstack15/librewolf-fedora-ff) output for copr.

### gtk3-classic
The [gtk3-classic](https://github.com/lah7/gtk3-classic) patch set is one of the main inputs to this process.

### gtk3
The debian [orig](http://deb.debian.org/debian/pool/main/g/gtk+3.0/gtk+3.0_3.24.29.orig.tar.xz) tarball is used for the build process.

The rpm uses the upstream gtk3 release asset.

## Alternatives
* [gtk3-stackrpms](https://gitlab.com/bgstack15/stackrpms/-/tree/master/gtk3-stackrpms) suite which suits my needs almost exactly, minus the file dialog type-to-find functionality.

## Dependencies
### deb script
To run the build script you need:
* rmadison
* git
* tar

To build gtk3, you need the standard set which is available in the `debian/control` file.

### rpm script
* git
* yum
* fedpkg-minimal

## References
* [gtk3-stackrpms](https://gitlab.com/bgstack15/stackrpms/-/tree/master/gtk3-stackrpms) suite
* [gtk3-nooverlayscrollbar](https://pkginfo.devuan.org/cgi-bin/policy-query.html?c=package&q=gtk3-nooverlayscrollbar&x=submit) in Devuan
* [luigifab/deb.sh](https://gist.github.com/luigifab/0fce786cdb93b5687069a82f490ea95e)

## Differences from upstreams
N/A. No direct upstreams
