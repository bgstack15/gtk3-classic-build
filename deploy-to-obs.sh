#!/bin/sh
# Startdate: 2023-01-08-1 09:37
# Purpose: make it a oneliner to get into the OBS
# Reference:
#    ublock-origin-combined/deploy-to-obs.sh notepadpp d-to-o
# Dependencies:
#    osc

devdir=~/dev
projectdir="${devdir}/gtk3-classic-build"
obsdir="${devdir}/osc/home:bgstack15:gtk3-classic/gtk3-classic"

# Flow
cd "${projectdir}"
"${projectdir}/gtk-classic-build-deb.sh"
echo "DONE WITH gtk-classic-build-deb"
tf="$( find . -maxdepth 1 -name 'gtk+3.0_*orig*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 )"
dsc_file="$( find . -maxdepth 1 -name 'gtk+3.0_*dsc' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"
debian_tar_file="$( find . -maxdepth 1 -name 'gtk+3.0_*debian*z' -printf '%T@ %f\n' | sort -n -k1 | awk '{print $NF}' | tail -n1 | cut -d' ' -f2 )"

# prep obs
cd "${obsdir}"
osc up
osc rm *
cd "${projectdir}"
cp -p "${dsc_file}" "${debian_tar_file}" "${tf}" "${obsdir}/"
cd "${obsdir}"
find . -mindepth 1 -maxdepth 1 ! -name '.osc' ! -name "${dsc_file}" ! -name "${debian_tar_file}" ! -name "${tf}" -delete
osc add *
osc commit # will prompt for commit name
