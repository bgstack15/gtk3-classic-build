#!/bin/sh
oldx="$( printf %s\\n "$-" | grep -q -e 'x' && echo YES )"
set +x # hide this cruft
test -z "${WORKDIR}" && WORKDIR="$( readlink -f . )"
export WORKDIR
RAW_DEB="$( rmadison gtk+3.0 | awk --field-separator='|' '{gsub(" ","");print $2,$2,$3}' | awk '{gsub("-[0-9]+","",$1);print}' | sort -r --sort=version | grep -v debug | head -n1)"
RAW="$( echo "${RAW_DEB}" | awk '{print $1}' )"
DEB="$( echo "${RAW_DEB}" | awk '{print $2}' )"
if ! test "$( cd "${WORKDIR}/gtk3classic" 2>/dev/null && git remote -v | grep -o 'origin	https://github.com/lah7/gtk3-classic' | head -n1 )" = "origin	https://github.com/lah7/gtk3-classic" ; then
   git clone "${GTK3CLASSIC_GIT}" "${WORKDIR}/gtk3classic"
else
   (
      cd "${WORKDIR}/gtk3classic"
      git checkout master
      git pull --all
   ) 1>/dev/null 2>&1
fi
CLASSIC="$( cd "${WORKDIR}/gtk3classic" ; git tag | sort -r --sort=version | head -n 1 )"
export DEB RAW CLASSIC
echo "CLASSIC=${CLASSIC}"
echo "DEB=${DEB}"
echo "RAW=${RAW}"
test "${oldx}" = "YES" && set -x
