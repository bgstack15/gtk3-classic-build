#!/bin/sh
# File: gtk-classic-build-deb.sh
# Location: https://gitlab.com/bgstack15/gtk3-classic-build
# Author: bgstack15
# Startdate: 2021-08-03 22:13
# SPDX-License-Identifier: GPL-3.0
# Title: Build dpkg-buildpackage assets for gtk3-classic
# Purpose: combine gtk+3.0 from debian and gtk3-classic into a .deb suite for Devuan Ceres
# History:
#    2023-01-11 switch package release to stackrpms
# Usage:
#    make
# References:
#    https://gist.github.com/luigifab/0fce786cdb93b5687069a82f490ea95e
#    https://launchpad.net/~luigifab/+archive/ubuntu/packages-gtk3-classic/+packages
#    https://launchpad.net/~luigifab/+archive/ubuntu/packages-gtk3-classic
#    https://github.com/lah7/gtk3-classic/releases
# Improve:
#    make the find-highest-version thing a menu?
# Dependencies:
#    rmadison, git, dpkg-buildpackage

test -z "${WORKDIR}" && WORKDIR="$( readlink -f . )"
export WORKDIR

GTK3CLASSIC_GIT=https://github.com/lah7/gtk3-classic
cd "${WORKDIR}"

if test -n "${RAW}" && test -n "${CLASSIC}" && test -n "${DEB}" ;
then
   echo "Using env vars:" ;
   (
      set -x
      deb_version="${DEB}"
      raw_version="${RAW}"
      classic_version="${CLASSIC}"
   )
   deb_version="${DEB}"
   raw_version="${RAW}"
   classic_version="${CLASSIC}"
else
   # original logic does all this comparison lookup, but it obviously fails when debian or gtk3-classic is behind/mismatched.
   #### Find versions of gtk3-classic available, which involves fetching the repo
   if ! test "$( cd "${WORKDIR}/gtk3classic" 2>/dev/null && git remote -v | grep -o 'origin	https://github.com/lah7/gtk3-classic' | head -n1 )" = "origin	https://github.com/lah7/gtk3-classic" ; then
      git clone "${GTK3CLASSIC_GIT}" "${WORKDIR}/gtk3classic"
   else
      (
         cd "${WORKDIR}/gtk3classic"
         git checkout master
         git pull --all
      )
   fi
   classic_vers="$( cd "${WORKDIR}/gtk3classic" ; git tag | sort -r --sort=version | head -n 20 )"
   echo "${classic_vers}" > "${WORKDIR}/classic_vers"

   #### Find versions of gtk+3.0 available to debian
   debian_vers="$( rmadison gtk+3.0 | awk --field-separator='|' '{gsub(" ","");print $2,$2,$3}' | awk '{gsub("-[0-9]+","",$1);print}' | sort -r --sort=version | grep -v debug )"
   echo "${debian_vers}" > "${WORKDIR}/debian_vers"

   #### Find highest matching version
   # the head -n1 makes it only the highest one. But anything in this list is a match between the two files.
   highest_ver="$( awk 'NR==FNR{a[$0];next} NR!=FNR{if($1 in a){print}}' "${WORKDIR}/classic_vers" "${WORKDIR}/debian_vers" | head -n1 )"
   raw_version="$( echo "${highest_ver}" | awk '{print $1}' )"
   deb_version="$( echo "${highest_ver}" | awk '{print $2}' )"
   classic_version="${raw_version}"

   # and $3 is the distro name like "experimental" which is not needed.
   echo "Found highest version: ${highest_ver}"
fi

#### Fetch debian sources
cd "${WORKDIR}"
for word in "${raw_version}.orig.tar.xz" "${deb_version}.debian.tar.xz" "${deb_version}.dsc";
do
   wget --no-clobber --content-disposition "http://deb.debian.org/debian/pool/main/g/gtk+3.0/gtk+3.0_${word}"
done

#### Fetch gtk3-classic sources
(
   cd "${WORKDIR}/gtk3classic"
   git checkout "${classic_version}"
)

#### Combine
# explode tarballs
tar -Jxf "${WORKDIR}/gtk+3.0_${raw_version}.orig.tar.xz"
cd "${WORKDIR}/gtk+-${raw_version}"
tar -Jxf "${WORKDIR}/gtk+3.0_${deb_version}.debian.tar.xz"
mv debian/changelog debian/changelog.orig
{
   echo "gtk+3.0 (${raw_version}-100+stackrpms) obs; urgency=medium"
   echo ""
   echo "  * Rebuild gtk3 with gtk3-classic patches"
   echo ""
   echo " -- B. Stack <bgstack15@gmail.com>  $( date "+%a, %d %b %+4Y %T %z" )"
   echo ""
} > debian/changelog
cat debian/changelog.orig >> debian.changelog
rm debian.changelog
cp -p "${WORKDIR}/gtk3classic/"*.patch debian/patches/
cp -p "${WORKDIR}/gtk3classic/"*.css debian/patches/
cat "${WORKDIR}/gtk3classic/series" >> debian/patches/series
sed -i -r debian/rules \
   -e 's/env -u LD_PRELOAD xvfb-run -a dh_auto_test/#env -u LD_PRELOAD xvfb-run -a dh_auto_test/' \
   -e '/NOMATCHFINDABLE/s/abcdefg/# added for 3.24.34 because LD_PRELOAD statement does nothing now/;' \
   -e '/run-tests\.sh/s/$/ || :/;'
sed -r -i 's/Build-Depends: /Build-Depends: libjpeg62-turbo-dev, /' debian/control
# remove patch for all
case "${raw_version}" in
   3.24.29|3.24.31)
      rm debian/patches/appearance__smaller-statusbar.patch debian/patches/file-chooser__places-sidebar.patch || :
      sed -r -i 's/(appearance__smaller-statusbar.patch|file-chooser__places-sidebar.patch)//g;' debian/patches/series
      sed -i -r debian/rules -e '2aDEB_BUILD_OPTIONS := nocheck'
      ;;
esac

#### Build .dsc and tarballs for OBS
echo "WORKHERE: build dsc and tarballs for OBS"
cd "${WORKDIR}/gtk+-${raw_version}"
dpkg-buildpackage --unsigned-source --unsigned-buildinfo --unsigned-changes -S

#### Optionally, perform build operation now.
#dpkg-buildpackage --unsigned-source --unsigned-buildinfo --unsigned-changes -B

#### Clean
test -z "${NO_CLEAN}" && rm -rf "${WORKDIR}/debian_vers" "${WORKDIR}/classic_vers" "${WORKDIR}/gtk+-${raw_version}"
